package raumschiff;

import java.util.ArrayList;

public class Raumschiff {

	private String schiffsName;
	private int photonenTorpedosAnzahl;
	private int energieVersorgungInProzent;
	private int zustandSchildeInProzent;
	private int zustandHuelleInProzent;
	private int zustandLebenserhaltungsSystemeInProzent;
	private int anzahlDroiden;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsVerzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		
	}
		
	public Raumschiff(int photonentorpedoAnzahl, int energieVersorgungInProzent, int zustandSchildeInProzent, int zustandHuelleInProzent, int zustandLebenserhaltungsSystemeInProzent, int anzahlDroiden, String schiffsName) {
		this.photonenTorpedosAnzahl = photonentorpedoAnzahl;
		this.energieVersorgungInProzent = energieVersorgungInProzent;
		this.zustandSchildeInProzent = zustandSchildeInProzent;
		this.zustandHuelleInProzent = zustandHuelleInProzent;
		this.zustandLebenserhaltungsSystemeInProzent = zustandLebenserhaltungsSystemeInProzent;
		this.anzahlDroiden = anzahlDroiden;
		
	}

	public String getSchiffsName() {
		return schiffsName;
	}

	public void setSchiffsName(String schiffsName) {
		this.schiffsName = schiffsName;
	}

	public int getPhotonenTorpedosAnzahl() {
		return photonenTorpedosAnzahl;
	}

	public void setPhotonenTorpedosAnzahl(int photonenTorpedosAnzahl) {
		this.photonenTorpedosAnzahl = photonenTorpedosAnzahl;
	}

	public int getEnergieVersorgungInProzent() {
		return energieVersorgungInProzent;
	}

	public void setEnergieVersorgungInProzent(int energieVersorgungInProzent) {
		this.energieVersorgungInProzent = energieVersorgungInProzent;
	}

	public int getZustandSchildeInProzent() {
		return zustandSchildeInProzent;
	}

	public void setZustandSchildeInProzent(int zustandSchildeInProzent) {
		this.zustandSchildeInProzent = zustandSchildeInProzent;
	}

	public int getZustandHuelleInProzent() {
		return zustandHuelleInProzent;
	}

	public void setZustandHuelleInProzent(int zustandHuelleInProzent) {
		this.zustandHuelleInProzent = zustandHuelleInProzent;
	}

	public int getZustandLebenserhaltungsSystemeInProzent() {
		return zustandLebenserhaltungsSystemeInProzent;
	}

	public void setZustandLebenserhaltungsSystemeInProzent(int zustandLebenserhaltungsSystemeInProzent) {
		this.zustandLebenserhaltungsSystemeInProzent = zustandLebenserhaltungsSystemeInProzent;
	}

	public int getAnzahlDroiden() {
		return anzahlDroiden;
	}

	public void setAnzahlDroiden(int anzahlDroiden) {
		this.anzahlDroiden = anzahlDroiden;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsVerzeichnis() {
		return ladungsVerzeichnis;
	}

	public void setLadungsVerzeichnis(ArrayList<Ladung> ladungsVerzeichnis) {
		this.ladungsVerzeichnis = ladungsVerzeichnis;
	}
	
	public void 
}
