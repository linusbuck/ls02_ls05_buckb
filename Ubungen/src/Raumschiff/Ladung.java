package Raumschiff;

import java.util.ArrayList;

public class Ladung {

	// Eigenschaften / Attribute

	private ArrayList<String> ladungsVerzeichnis = new ArrayList<String>();

	// Konstruktoren

	public Ladung() {
		setLadungsVerzeichnis("leer", 0);
	}

	public Ladung(String bezeichnung, int menge) {
		setLadungsVerzeichnis(bezeichnung, menge);

	}

	// Methoden

	public ArrayList<String> getLadungsVerzeichnis() {
		return ladungsVerzeichnis;
	}

	public void setLadungsVerzeichnis(String Ladung, Integer Anzahl) {
		// System.out.println("Ladung: " + Ladung + " Anzahl: " + Anzahl);
		String ladungGesamt = Ladung + ";" + Anzahl;
		ladungsVerzeichnis.add(ladungGesamt);

	}

	public void gibLadungsverzeichnisAus(ArrayList<String> Ladung) {
		System.out.println("Ladungsverzeichnis wird ausgegeben: ");
		for (String beladung : Ladung) {
			String[] beladungMitAnzahl = beladung.split(";");
			if (beladungMitAnzahl[1].equals("0")) {
				System.out.println(beladungMitAnzahl[0]);
			} else {
				System.out.println(beladungMitAnzahl[1] + " " + beladungMitAnzahl[0]);
			}

		}

	}

}