package Raumschiff;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Sh'Raan", "Uran", 200);
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Genorex", "Fusionszellen", 5);
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Koraga", "Forschungssonde", 35);

		vulkanier.setLadungsVerzeichnis("Photonentorpedo", 3);
		vulkanier.nachricht("sprecht euer letztes Gebet");
		
		klingonen.setLadungsVerzeichnis("Blutwein", 200);
		klingonen.photonenTorpedosSchiessen(romulaner, klingonen);
		klingonen.doZustand();
		klingonen.setLadungsVerzeichnis("Nuka-Cola", 50);
		klingonen.getLadungsVerzeichnis();

		romulaner.setLadungsVerzeichnis("Dunkle Materie", 2);
		romulaner.phaserkanoneSchiessen(klingonen, romulaner);
		romulaner.setLadungsVerzeichnis("Chemische Elemente", 50);
		romulaner.doZustand();


	}

}
